#!/bin/sh
#
# /etc/rc.d/rc.sysinit.micro - run once at boot time
#
# Taken in part from standart ALT Linux rc.sysinit
#

# Don't do coredumps
ulimit -Sc 0 >/dev/null 2>&1

trap '' SIGPIPE

# Rerun ourselves through initlog
if [ -z "$IN_INITLOG" -a -x /sbin/initlog ]; then
	exec /sbin/initlog $INITLOG_ARGS -r /etc/rc.d/rc.sysinit.micro
fi

WITHOUT_RC_COMPAT=1

# Source function library
. /etc/init.d/functions

# Fix console loglevel
dmesg -n "$LOGLEVEL"

HOSTNAME=`hostname`

# Read in config data
if ! SourceIfNotEmpty /etc/sysconfig/network; then
	NETWORKING=no
fi

if [ -z "$HOSTNAME" -o "$HOSTNAME" = "(none)" ]; then
	HOSTNAME=localhost
fi

PrintMargin()
{
	if [ "$BOOTUP" != serial ]; then
		local n
		n=`echo -n "$*" |wc -c` 2>/dev/null &&
			tput hpa $(( (COLUMNS - n)/2 ))
	fi
}

# Print a banner. ;)
echo
WELCOME="Welcome to "
BANNER="Uniset Mini Linux"
PrintMargin "$WELCOME$BANNER"
echo -n "$WELCOME"
SETCOLOR_INFO
echo -n "$BANNER"
SETCOLOR_NORMAL
echo

# Initialize bootsplash subsystem.
splash_init 10
splash_update start 1

# Remount the root filesystem
splash_update remount 2

# Mount /proc (done here so volume labels can work with fsck)
action "Mounting proc filesystem:" mount none /proc -n -t proc
chgrp proc /proc >/dev/null 2>&1 ||:

# Mount /sys where appropriate
if grep -wqs sysfs /proc/filesystems; then
	action "Mounting sys filesystem:" mount -n -t sysfs sysfs /sys
fi

if grep -iwqs rwmode /proc/cmdline; then
	action "Remounting root filesystem in read-write mode:" /etc/rc.d/init.d/rwmode init rw
else
	action "Remounting root filesystem in READ-ONLY mode:" /etc/rc.d/init.d/rwmode init
fi

action "Mounting tmpfs filesystem:" mount /tmp

# Enter root and /proc into mtab. (pixel) also added /initrd/loopfs for loopback root
mount -f /initrd/loopfs 2>/dev/null

# Set the system clock (when /etc/adjtime is missing)
[ -s /etc/adjtime ] || /etc/init.d/clock start

# Activate swap
if grep "^[^#].*swap" /etc/fstab >/dev/null; then
	action "Activating swap partitions:" swapon -a -e
fi

# Set the hostname
action "Setting hostname $HOSTNAME:" hostname "$HOSTNAME"

# Configure kernel parameters
splash_update kernel 4
action "Configuring kernel parameters:" sysctl -e -p /etc/sysctl.conf

splash_update system 5

# Needs replace to user specific settings
mkdir -p /tmp/.private/root && chown root:root /tmp/.private/root
mkdir -p /tmp/.private/guest && chown guest:guest /tmp/.private/guest

# Load modules
if [ -f /proc/modules -a -x /sbin/modprobe ] &&
   ! grep -iwqs nomodules /proc/cmdline; then
	MODPROBE=/sbin/modprobe
	/etc/rc.d/scripts/load_modules
else
	MODPROBE=/bin/true
fi

# Initialize devices
splash_update devices 6
RUN_UDEV=
udevd_exe=/etc/init.d/udevd
[ ! -x "$udevd_exe" ] ||
	grep -iwqs noudev /proc/cmdline ||
	RUN_UDEV=1

# Start udev
splash_update udev 7
if [ -n "$RUN_UDEV" ]; then
	RUN_FROM_SYSINIT=1 "$udevd_exe" start
	[ -c /dev/rtc ] || $MODPROBE rtc >/dev/null 2>&1
else
	action "Mounting device filesystem in read-write mode:" /etc/rc.d/init.d/rwmode dev
fi

# Mount all other filesystems
splash_update mount 8
action 'Mounting local filesystems:' mount -a -t noproc,nodevpts,nousbfs -O no_netdev

# Update chrooted environments
splash_update chrooted 9
if [ -x /usr/sbin/update_chrooted ]; then
	action "Updating chrooted environments:" /usr/sbin/update_chrooted conf lib
fi

# Set system font
ExecIfExecutable /sbin/setsysfont >/dev/null 2>&1

# Now that we have all of our basic modules loaded and the kernel going,
# let's dump the syslog ring somewhere so we can find it later
dmesg >/var/log/dmesg
