Name: startup-micro
Version: 0.3
Release: alt4.2

Summary: The system startup scripts for microboot
License: GPL
Group: System/Base
BuildArch: noarch
Packager: Evgeny Sinelnikov <sin@altlinux.ru>

Source: %name-%version.tar
URL: http://git.etersoft.ru/projects/asu/startup-micro.git

Requires: startup

%description
This package contains scripts used to boot embedded systems.
To use install the package and make link /etc/inittab to /etc/inittab.micro.
Use service rwmode on|off on enable/disable rw mode in the system.

%prep
%setup

%install
mkdir -p -- %buildroot{%_sysconfdir,%_sysconfdir/rc.d,%_initdir}

install -m755 rwmode %buildroot%_initdir/
install -p -m644 inittab.micro inittab.micro.gui modules.micro %buildroot%_sysconfdir/
install -m755 rc.sysdown rc.sysinit.micro %buildroot%_sysconfdir/rc.d/

%files
%_sysconfdir/*.micro*
%_sysconfdir/rc.d/rc.*
%_initdir/rwmode

%changelog
* Mon Jul 11 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt4.2
- Pack inittab.micro.gui

* Mon Jul 11 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt4.1
- Install inittab.micro.gui

* Mon Jul 11 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt4
- Add inittab.micro.gui for runlevel 5.

* Mon Jul 11 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt3
- Add check dir with mountpoint.
- Revert set system font.

* Sun Jun 26 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt2
- Replace device initialization of rwmode without udev.

* Sat Jun 25 2011 Evgeny Sinelnikov <sin@altlinux.ru> 0.3-alt1
- Add udev support.
- Check fstab for swapon.
- Add initialization for rwmode.

* Sat Oct 10 2009 Vitaly Lipatov <lav@altlinux.ru> 0.2-alt1
- fix tarball name

* Sat Oct 03 2009 Vitaly Lipatov <lav@altlinux.ru> 0.1.0-alt5
- rename service to rwmode
- add ram disk umount support in rwmode script
- use rwmode script in  rc.sysinit for initial remount

* Fri Jun 19 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1.0-alt4
- Change unionfs to aufs

* Thu Jun 04 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1.0-alt3
- Fix silly bug of running rc script with initlog

* Mon Jun 01 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1.0-alt2
- Clean scripts

* Thu May 28 2009 Evgeny Sinelnikov <sin@altlinux.ru> 0.1.0-alt1
- First build for ALT Linux
- Based on startup-nanolive 0.1.0-alt1
